import React, { Component } from 'react';
import './App.css';

import ReduxClock from './components/stateful/ReduxClock';

function App() {
  return (
    <div className="App" >
      <ReduxClock />
    </div>
  );
}

export default App;
