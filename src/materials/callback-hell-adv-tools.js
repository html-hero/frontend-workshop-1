
const vendors = [
  { name: 'Gramedia', address: 'Jl. Gramedia' },
  { name: 'Sampoerna', address: 'Jl. Sampoerna' },
];

exports.lookupVendor = (name, done) => {
  const result = vendors.find(vendor => vendor.name === name);
  if (result) return done(null, result);
  return done(new Error('vendor not found'), null);
};

const distributors = [
  { name: 'CV. Terang Benderang', address: 'Jl. Matahari' },
  { name: 'CV. Gelap Gulita', address: 'Jl. Entah Dimana' },
];

exports.lookupDistributor = (name, done) => {
  const result = distributors.find(distributor => distributor.name === name);
  if (result) return done(null, result);
  return done(new Error('distributor not found'), null);
};

const stocks = [
  { name: 'Buku', vendor: 'Gramedia', distributor: 'CV. Terang Benderang' },
  { name: 'Dji Sam Soe', vendor: 'Sampoerna', distributor: 'CV. Gelap Gulita' },
];

exports.lookupStock = (name, done) => {
  const result = stocks.find(stock => stock.name === name);
  if (result) return done(null, result);
  return done(new Error('stock not found'), null);
};

