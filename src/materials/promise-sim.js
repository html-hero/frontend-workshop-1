
function theExecutor(text, done) {
  done(null, text);
}

function theExecutorPromise(text) {
  return new Promise((resolve, reject) => {
    theExecutor(text, (err, result) => {
      if (err) return reject(err);
      return resolve(result);
    });
  });
}

theExecutor('Algojo', (err, result) => {
  console.log(err, result);
});

theExecutorPromise('Algojo Promise')
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  });
