
function theCallback(text) {
  console.log(text);
}

function theExecutor(text, done) {
  done(text);
}

theExecutor('Algojo', theCallback);
