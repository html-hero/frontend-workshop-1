
const util = require('util');

function theExecutor(text, done) {
  done(null, text);
}

const theExecutorPromise = util.promisify(theExecutor);

theExecutor('Algojo', (err, result) => {
  console.log(err, result);
});

theExecutorPromise('Algojo Promise')
  .then(result => console.log(result))
  .catch(err => console.log(err));
