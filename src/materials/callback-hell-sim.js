
function theCallbackFirst(text, done) {
  done(text);
}

function theCallbackSecond(text, done) {
  done(text);
}

function theCallbackThird(text, done) {
  done(text);
}

function theExecutor(text, done) {
  done(text);
}

theExecutor('Algojo', function (text) {
  theCallbackFirst(text, function (text) {
    theCallbackSecond(text, function (text) {
      theCallbackThird(text, function (text) {
        console.log(text);
      });
    });
  });
});
