const stocks = [
  { name: 'Cigarette', available: false, price: 20000 },
  { name: 'Mineral Water', available: true, price: 2000 },
  { name: 'Poki', available: true, price: 5000 },
];

const lookupItem = item => (
  // return empty object if not found
  stocks.find((stock) => item.name === stock.name && stock.available) || {}
);

// get total price
const reducer = (total, price) => total + (price || 0);

const scanItems = (items, done) => {
  const total = items.map(item => lookupItem(item).price).reduce(reducer, 0);
  if (total > 0) return done(null, total);
  return done(new Error('Zero Price'), null);
}

const purchaseItems = [{ name: 'Cigarette' }, { name: 'Poki' }];
scanItems(purchaseItems, (err, price) => console.log(err, price));

