
const util = require('util');

function theExecutor(text, done) {
  done(null, text);
}

function thePunisher(text, done) {
  done(null, text);
}

function theCrazyExecutor(text, done) {
  done(new Error('Going Crazy !@#$%@#@^##^#&&#'), null);
}

const theExecutorPromise = util.promisify(theExecutor);
const thePunisherPromise = util.promisify(thePunisher);
const theCrazyExecutorPromise = util.promisify(theCrazyExecutor);

theExecutor('Algojo', (err, result) => {
  console.log(err, result);
});

theExecutorPromise('Algojo Promise')
  .then((result) => {
    console.log('the executor:', result);
    thePunisherPromise(result);
  })
  .then((result) => {
    console.log('the punisher:', result);
    theCrazyExecutorPromise(result);
  })
  .then((result) => {
    console.log('the crazy executor:', result);
    return true;
  })
  .catch((err) => {
    console.log(err);
  });
