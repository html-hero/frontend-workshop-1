
const util = require('util');

const theExecutor = (text, done) => done(null, text);
const theExecutorPromise = util.promisify(theExecutor);

theExecutorPromise('Algojo executed you!');
console.log('Run... run... for your life!!!!');
