
const {
  lookupVendor,
  lookupDistributor,
  lookupStock,
} = require('./callback-hell-adv-tools');

const getItemDetail = (items, done) => {
  const result = [];
  for (let i = 0; i < items.length; i++) {
    const detail = {};
    lookupStock(items[i], (err, stock) => {
      if (err) {
        return done(err);
      }

      detail.stock = stock;

      lookupVendor(stock.vendor, (err, vendor) => {
        if (err) {
          return done(err);
        }

        detail.vendor = vendor;

        lookupDistributor(stock.distributor, (err, distributor) => {
          if (err) {
            return done (err);
          }

          detail.distributor = distributor;
        });
      });
    });

    result.push(detail);
  }

  return done(null, result);
};

const items = ['Buku', 'Dji Sam Soe'];
getItemDetail(items, (err, result) => {
  console.log(err, result);
});



