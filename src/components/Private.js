
import React, { Component } from 'react';
import Clock from './Clock';

export default class PrivateClock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ticking: new Date(),
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({ ticking: new Date() });
    }, 1000);
  }

  render() {
    return (
      <Clock date={this.state.ticking} />
    );
  }
}
