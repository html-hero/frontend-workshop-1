
import React, { Component } from 'react';

export default class PropComponent extends Component {
  render() {
    return (
      <div>{JSON.stringify(this.props)}</div>
    );
  }
}
