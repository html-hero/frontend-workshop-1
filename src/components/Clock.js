
import React from 'react';

export default function Clock({ date }) {
  return (
    <h2>It is {date.toString()}.</h2>
  );
}
