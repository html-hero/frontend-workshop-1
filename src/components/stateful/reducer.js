import { TYPES } from './actions'

const initialStates = {
  clock: {
    date: new Date(),
  },
}

export const clock = (state = initialStates.clock, action) => {
  switch (action.type) {
    case TYPES.UPDATE_DATE:
      return Object.assign({}, state, { date: action.date })
    default:
      return state
  }
}
