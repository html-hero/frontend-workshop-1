
export const TYPES = {
  UPDATE_DATE: 'UPDATE_DATE'
}

export const actions = {
  setDate: (date) => ({ type: TYPES.UPDATE_DATE, date })
}
