import React from 'react';
import { connect } from 'react-redux';

import { actions } from './actions';

function Clock({ date }) {
  return (
    <h2>It is {date.toString()}.</h2>
  )
}

function ReduxClock({ date, dispatch }) {
  setInterval(() => {
    dispatch(actions.setDate(new Date()));
  }, 1000);

  return (
    <Clock date={date} />
  )
}

const mapStateToProps = (state) => ({
  date: state.clock.date
})

export default connect(mapStateToProps)(ReduxClock)


