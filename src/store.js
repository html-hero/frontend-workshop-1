import { createStore, combineReducers } from 'redux';

import { clock } from './components/stateful/reducer';

const rootReducer = combineReducers({
  clock,
});

export default createStore(rootReducer);
